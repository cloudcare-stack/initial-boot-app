//package com.revature.controllers;
//
//import com.revature.models.Diagnosis;
//import com.revature.services.DiagnosisService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/diagnosis")
//@CrossOrigin
//public class DiagnosisController {
//
//    @Autowired
//    DiagnosisService diagnosisService;
//
//    @GetMapping // this method handles GET /diagnosis
//    public List<Diagnosis> getDiagnosisHandler(@RequestParam(required = false, value="name") String nameParam){
//        System.out.println("Name Parameter: " + nameParam);
//        if(nameParam == null){
//            return diagnosisService.getAllDiagnosis();
//        } else {
//            return diagnosisService.findDiagnosisByName(nameParam);
//        }
//    }
//
//}
