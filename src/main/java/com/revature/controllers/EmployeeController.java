package com.revature.controllers;

import com.revature.models.Employee;
import com.revature.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
@CrossOrigin
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping
    public List<Employee> getEmployeeHandler(@RequestParam(required = false, value="name") String nameParam){
        System.out.println("Name Parameter: " + nameParam);

        //if(nameParam == null){
            return employeeService.getAllEmployees();
        //}
//        else {
////            return employeeService.findEmployeeByName(nameParam);
//            return null;
//        }
    }

}
