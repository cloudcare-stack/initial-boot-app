package com.revature.services;

import com.revature.models.Employee;
import com.revature.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        super();
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees(){
        return this.employeeRepository.findAll();
    }

//    public List<Employee> findEmployeeByName(String name){
//        return employeeRepository.findEmployeeByName(name);
//    }

//    public String authenticateUser(String name){
//        return employeeRepository.authenticateUser(name);
//    }
}
