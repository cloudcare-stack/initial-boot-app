//package com.revature.services;
//
//import com.revature.models.Diagnosis;
//import com.revature.repositories.DiagnosisRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class DiagnosisService {
//
//    @Autowired
//    private DiagnosisRepository repository;
//
//    public DiagnosisService(){
//
//    }
//
//    public List<Diagnosis> getAllDiagnosis(){
//        return repository.findAll();
//    }
//
//    public List<Diagnosis> findDiagnosisByName(String name){
//        return repository.findDiagnosisByName(name);
//    }
//}
