//package com.revature.models;
//
//import java.time.LocalDate;
//import java.util.Objects;
//
//public class Diagnosis {
//
//    private int id;
//    private String first;
//    private String last;
//    private LocalDate birthday;
//    private LocalDate service;
//    private String description;
//    private int employee_id;
//
//    public Diagnosis() {
//        super();
//    }
//
//    public Diagnosis(int id, String first, String last, LocalDate birthday, LocalDate service, String description, int employee_id) {
//        this.id = id;
//        this.first = first;
//        this.last = last;
//        this.birthday = birthday;
//        this.service = service;
//        this.description = description;
//        this.employee_id = employee_id;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getFirst() {
//        return first;
//    }
//
//    public void setFirst(String first) {
//        this.first = first;
//    }
//
//    public String getLast() {
//        return last;
//    }
//
//    public void setLast(String last) {
//        this.last = last;
//    }
//
//    public LocalDate getBirthday() {
//        return birthday;
//    }
//
//    public void setBirthday(LocalDate birthday) {
//        this.birthday = birthday;
//    }
//
//    public LocalDate getService() {
//        return service;
//    }
//
//    public void setService(LocalDate service) {
//        this.service = service;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public int getEmployee_id() {
//        return employee_id;
//    }
//
//    public void setEmployee_id(int employee_id) {
//        this.employee_id = employee_id;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Diagnosis)) return false;
//        Diagnosis diagnosis = (Diagnosis) o;
//        return getId() == diagnosis.getId() && getEmployee_id() == diagnosis.getEmployee_id() && Objects.equals(getFirst(), diagnosis.getFirst()) && Objects.equals(getLast(), diagnosis.getLast()) && Objects.equals(getBirthday(), diagnosis.getBirthday()) && Objects.equals(getService(), diagnosis.getService()) && Objects.equals(getDescription(), diagnosis.getDescription());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getId(), getFirst(), getLast(), getBirthday(), getService(), getDescription(), getEmployee_id());
//    }
//
//    @Override
//    public String toString() {
//        return "Diagnosis{" +
//                "id=" + id +
//                ", first='" + first + '\'' +
//                ", last='" + last + '\'' +
//                ", birthday=" + birthday +
//                ", service=" + service +
//                ", description='" + description + '\'' +
//                ", employee_id=" + employee_id +
//                '}';
//    }
//}
