package com.revature.models;

public enum EmployeeRole {
    DOCTOR, NURSE
}
