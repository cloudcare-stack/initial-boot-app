package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="hospitalemployee")
public class Employee {

    @Id
    @Column(name="employee_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "first_name")
    private String first;
    @Column(name = "last_name")
    private String last;
    @Column(name = "employee_role")
    private EmployeeRole employee_role;
    @Column(name = "email")
    private String username;
    @Column(name="pass")
    private String password;
    @Column(name="status")
    private boolean status;

    public Employee() {
        super();
    }

    public Employee(int id, String first, String last, EmployeeRole employeeRole, String username, String password, boolean status) {
        this.id = id;
        this.first = first;
        this.last = last;
        this.employee_role = employeeRole;
        this.username = username;
        this.password = password;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public EmployeeRole getEmployeeRole() {
        return employee_role;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employee_role = employeeRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getId() == employee.getId() && isStatus() == employee.isStatus() && Objects.equals(getFirst(), employee.getFirst()) && Objects.equals(getLast(), employee.getLast()) && getEmployeeRole() == employee.getEmployeeRole() && Objects.equals(getUsername(), employee.getUsername()) && Objects.equals(getPassword(), employee.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirst(), getLast(), getEmployeeRole(), getUsername(), getPassword(), isStatus());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + first + '\'' +
                ", last='" + last + '\'' +
                ", employee_role=" + employee_role +
                ", email='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                '}';
    }
}
