const xhr = new XMLHttpRequest();

xhr.open("GET","http://localhost:8080/employees");

xhr.onreadystatechange = function(){
    if(xhr.readyState===4){
        // obtain the customer data from the response
        const responseBody = xhr.response;
        // convert JSON response body into JS objects
        const employeeArray = JSON.parse(responseBody);
        renderEmployees(employeeArray);
    }
}

xhr.send();

// renderCustomers should take an array of JS objects
    // and render list items on our web page, based on each item
function renderCustomers(employees){

    const customerList = document.getElementById("employee-list");

    for(let employee of employees){
        const newListItem = document.createElement("li");
        newListItem.classList.add("list-group-item");
        newListItem.innerText = ` ${employee.name} --- ${employee.email} `;
        console.log(newListItem);
        employeeList.appendChild(newListItem);
    }

}

